//Soal no 1
var nama1 = "";
var peran1 = "Werewolf";

if (nama1 == ""){
    console.log("Nama harus diisi!");
}else if (peran1 == ""){
    console.log("Halo " + nama1 + ", Pilih peranmu untuk memulai game!");
}else if (peran1 == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama1); 
    console.log("Halo Penyihir " + nama1 + ", kamu dapat melihat siapa yang menjadi werewolf!");
}else if (peran1 == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama1); 
    console.log("Halo Guard " + nama1 + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
}else if (peran1 == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama1); 
    console.log("Halo Werewolf " + nama1 + ", Kamu akan memakan mangsa setiap malam!");
};

console.log("================================================================================");

var nama2 = "Jaka";
var peran2 = "";

if (nama2 == ""){
    console.log("Nama harus diisi!");
}else if (peran2 == ""){
    console.log("Halo " + nama2 + ", Pilih peranmu untuk memulai game!");
}else if (peran2 == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama2); 
    console.log("Halo Penyihir " + nama2 + ", kamu dapat melihat siapa yang menjadi werewolf!");
}else if (peran2 == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama2); 
    console.log("Halo Guard " + nama2 + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
}else if (peran2 == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama2); 
    console.log("Halo Werewolf " + nama2 + ", Kamu akan memakan mangsa setiap malam!");
};

console.log("================================================================================");

var nama3 = "Alek";
var peran3 = "Penyihir";

if (nama3 == ""){
    console.log("Nama harus diisi!");
}else if (peran3 == ""){
    console.log("Halo " + nama3 + ", Pilih peranmu untuk memulai game!");
}else if (peran3 == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama3); 
    console.log("Halo Penyihir " + nama3 + ", kamu dapat melihat siapa yang menjadi werewolf!");
}else if (peran3 == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama3); 
    console.log("Halo Guard " + nama3 + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
}else if (peran3 == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama3); 
    console.log("Halo Werewolf " + nama3 + ", Kamu akan memakan mangsa setiap malam!");
};

console.log("================================================================================");

var nama4 = "Boy";
var peran4 = "Guard";

if (nama4 == ""){
    console.log("Nama harus diisi!");
}else if (peran4 == ""){
    console.log("Halo " + nama4 + ", Pilih peranmu untuk memulai game!");
}else if (peran4 == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama4); 
    console.log("Halo Penyihir " + nama4 + ", kamu dapat melihat siapa yang menjadi werewolf!");
}else if (peran4 == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama4); 
    console.log("Halo Guard " + nama4 + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
}else if (peran4 == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama4); 
    console.log("Halo Werewolf " + nama4 + ", Kamu akan memakan mangsa setiap malam!");
};

console.log("================================================================================");

var nama5 = "Lexy";
var peran5 = "Werewolf";

if (nama5 == ""){
    console.log("Nama harus diisi!");
}else if (peran5 == ""){
    console.log("Halo " + nama5 + ", Pilih peranmu untuk memulai game!");
}else if (peran5 == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama5); 
    console.log("Halo Penyihir " + nama5 + ", kamu dapat melihat siapa yang menjadi werewolf!");
}else if (peran5 == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama5); 
    console.log("Halo Guard " + nama5 + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
}else if (peran5 == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama5); 
    console.log("Halo Werewolf " + nama5 + ", Kamu akan memakan mangsa setiap malam!");
};

// Soal NO 2

console.log("===================================SOAL NO 2=============================================");

var hari = 21; 
var bulan = 7; 
var tahun = 1945;

switch(bulan){
    case 1: {console.log(hari + " Januari " + tahun); break; };
    case 2: {console.log(hari + " Februari " + tahun); break; };
    case 3: {console.log(hari + " Maret " + tahun); break; };
    case 4: {console.log(hari + " April " + tahun); break; };
    case 5: {console.log(hari + " Mei " + tahun); break; };
    case 6: {console.log(hari + " Juni " + tahun); break; };
    case 7: {console.log(hari + " Juli " + tahun); break; };
    case 8: {console.log(hari + " Agustus " + tahun); break; };
    case 9: {console.log(hari + " September " + tahun); break; };
    case 10: {console.log(hari + " Oktober " + tahun); break; };
    case 11: {console.log(hari + " November " + tahun); break; };
    case 12: {console.log(hari + " Desember " + tahun); break; };
}