
console.log("==============No 1================");
console.log(" ");
function arrayToObject(data) {
    jumlahOrang= data.length;
    var orang = {};
    var now = new Date()
    var thisYear = now.getFullYear();
    if(data.length <= 0){
        return console.log("")
    }
    for (var i = 0; i<jumlahOrang; i++ ){
        var namaDepan = data[i][0]
        var namaBelakang = data[i][1]
        var jenis = data[i][2]
        var tahun = data[i][3]
        var umur = thisYear-tahun;
        if (tahun==null || tahun>thisYear){
            umur="Invalid Birth Year";
        }
        orang.firstName= namaDepan;
        orang.lastName= namaBelakang;
        orang.gender= jenis;
        orang.age= umur;           
        console.log(i+1+"." + " " + namaDepan + " " + namaBelakang)
        console.log(orang)

    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people);
console.log(" "); 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
arrayToObject([]) // ""

console.log(" ");    
console.log("==============No 2================");
console.log(" ");

function shoppingTime(memberId, money) {
    
    

    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000){
        return "Mohon Maaf , uang tidak cukup"
    }else{
        objectShopping={};
        var changeMoney= money;
        var listPurchased= [];
        banyak=0;
        for(var i = 0; changeMoney>=50000 && banyak==0; i++){
            if (changeMoney>=1500000){
                changeMoney= changeMoney-1500000;
                listPurchased.push("Sepatu Stacattu");
            }else if(changeMoney>=500000){
                changeMoney= changeMoney-500000;
                listPurchased.push("Baju Zoro")
            }else if(changeMoney>=250000){
                changeMoney= changeMoney-250000;
                listPurchased.push("Baju H&N")
            }else if(changeMoney>=175000){
                changeMoney= changeMoney-175000;
                listPurchased.push("Sweater Uniklooh")
            }else if(changeMoney>=50000){
                for (var j= 0; j<=listPurchased.length-1; j++){
                    if(listPurchased[j]== "Casing Handphone"){
                        banyak +=1;
                    }
                }if(banyak==0){
                    changeMoney= changeMoney-50000;
                    listPurchased.push("Casing Handphone")
                } 
            }
        }
        
        objectShopping={
            memberId: memberId,
            money: money,
            listPurchasedd: listPurchased,
            changeMoney: changeMoney
        }
        return objectShopping
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log(" ");    
console.log("==============No 3================");
console.log(" ");

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var newArray = [];
    if(arrPenumpang.length <= 0){
        return console.log([])
    }
    
    for(var i= 0; i<arrPenumpang.length; i++){
        var object = {};
        var ruteAsal = [i][1]
        var ruteTujuan = [i][2]
        var indexAsal = 0;
        var indexTujuan = 0;
        for(var j =0; j<rute.length; j++){
            if (rute[j]==ruteAsal){
                indexAsal = j;
            }else if (rute[j]==ruteTujuan){
                indexTujuan = j;
            }
        }
        var bayar = (indexTujuan-indexAsal) * 2000;


        object.penumpang= arrPenumpang[i][0]
        object.naikDari= arrPenumpang[i][1]
        object.tujuan= arrPenumpang[i][2]
        object.bayar= bayar
        newArray.push(object);
        
    }
    return newArray;
    
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]