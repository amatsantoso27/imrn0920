console.log("==============Soal No 1==============");

function range(startNum, finishNum){
    var array = [];
    // var startNum=0;
    // var finishNum=0;
    if (startNum>=finishNum){
        for (var c=startNum; c>=finishNum; c--){
            array.push(c);
        }
    } else if(startNum<=finishNum){
        for (var b=startNum; b<=finishNum; b++){
            array.push(b);
        }
    } else if(startNum == 1){
        return -1
    } else {
        return -1
    } 
    return array;
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log("==============Soal No 2==============");

function rangeWithStep(startNum, finishNum, step){
    var array2=[];

    if (startNum<=finishNum){
        for(var d = startNum; d<=finishNum; d+=step){
            array2.push(d);
        }
    } else {
        for(var d = startNum; d>=finishNum; d-=step){
            array2.push(d);
        }
    } 

    return array2;
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log("==============Soal No 3==============");

function sum(startNum, finishNum, step=1){
    var array3=[];

    if(startNum<=finishNum){
        for(var e = startNum; e <= finishNum; e+=step){
            array3.push(e);
        }   
    }else if (startNum>=finishNum){
        for(var e=startNum; e>=finishNum; e-=step){
            array3.push(e);
        }
    }else if(startNum){
        return startNum;
    }else {
        return 0;
    }
    var total = 0;

    for(var f = 0; f<array3.length; f++){
        total = total + array3[f];
    }
    return total;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("==============Soal No 4==============");


var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"], // 0
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"], // 1
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"], // 2
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"] // 3
    ];

function dataHandling(data) {
    var panjangData = data.length
    for (var i = 0; i < panjangData; i++) {
        var id = 'Nomor ID: ' + data[i][0]
        var nama = 'Nama Lengkap: ' + data[i][1]
        var ttl = 'TTL: ' + data[i][2] + ' ' + data[i][3]
        var hobi = 'Hobi: ' + data[i][4]
        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)
        console.log()
    } 
}

dataHandling(input);

console.log("==============Soal No 5==============");

function balikKata(kata) {
    // var oldKata = kata
    var kataBaru = '';
    for (var i = kata.length - 1; i >= 0; i--) { // length =11; index = 10
        kataBaru += kata[i]
    }
    return kataBaru;
}

  console.log(balikKata("Kasur Rusak")) // kasuR rusaK
  console.log(balikKata("SanberCode")) // edoCrebnaS
  console.log(balikKata("Haji Ijah")) // hajI ijaH
  console.log(balikKata("racecar")) // racecar
  console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("==============Soal No 6==============");
["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
