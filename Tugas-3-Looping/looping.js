console.log("----------------Looping Pertama------------------");
var angka = 2;
while (angka <= 20) {
    console.log(angka + " - I love coding");
    angka=angka+2;
}
console.log("------------------Looping Kedua----------------");
var angka= 20;
while (angka>=2) {
    console.log(angka + " - I will become a mobile developer");
    angka=angka-2;
}
console.log("------------------Soal No 2------------------");
for(var nomor=1; nomor <= 20; nomor++){
    if(nomor%3==0 && nomor%2!=0){
        console.log(nomor + " - I Love Coding");
    }else if(nomor%2==0){
        console.log(nomor + " - Berkualitas");
    }else if(nomor%2!=0){
        console.log(nomor + " - Santai");
    }
}
console.log("------------------Soal No 3-----------------");
var ulang3=1;
while(ulang3<=4){
    for(ulang3j=1; ulang3j<=8; ulang3j++){
        process.stdout.write("#")
    }
    console.log("")
    ulang3++;
}
console.log("------------------Soal No 4-----------------");

for (var i = 1; i<=7; i++){
    for(var j = i; j>=1; j--){
        process.stdout.write("#");
    }console.log("");
}

console.log("------------------Soal No 5-----------------");

for (var k =1; k<=8; k++){
    for(var l=1; l<=4; l++){
        if(k%2==0){            
            process.stdout.write(" ");
            process.stdout.write("#");
        }else if(k%2==1){
            process.stdout.write("#");
            process.stdout.write(" ");
        }        
    }console.log(" ");
}