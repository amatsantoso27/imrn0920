var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
t= 10000;
i=0;
function jalankan(){
    readBooksPromise(t, books[i])
    .then(function (fullfilled){
        t= fullfilled
        i++
        if (i < books.length)
        jalankan()
        
    })
    .catch(function (error){
        t= error
        i++
        if (i < books.length)
        jalankan()
        
    })
}

jalankan()
