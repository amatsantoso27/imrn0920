/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './Tugas/Quiz3/Route';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
