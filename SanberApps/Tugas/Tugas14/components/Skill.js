import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import ItemSkill from './ItemSkill';
import data from '../skillData.json';

export default class Skill extends React.Component{
  render() {
    return(
      <View style={styles.container}>
        <Image source={require('../images/logo.png')} style={{alignSelf: 'flex-end', height: 50, width: 180, margin: 10}} />
        <View style={styles.headHai}>
          <Icon style={{color: '#3ec6ff'}} solid name="user-circle" size={50} />
            <View  style={styles.headHaiDalam}>
              <Text style={styles.textHai}>Hai,</Text>
              <Text style={styles.textHai2}>Achmad Santoso</Text>
            </View>
        </View> 
        <View style={styles.head2}>
          <Text style={styles.textHead2}>SKILL</Text>
        </View>
        <View style={styles.kotak1}>
          <View>
            <Text style={styles.textKotak1}>Library / Framework</Text>
          </View>
          <View>
            <Text style={styles.textKotak1}>Bahasa Pemrograman</Text>
          </View>
          <View>
            <Text style={styles.textKotak1}>Teknologi</Text>
          </View>
        </View>
        
          <View style={styles.list}>
            <FlatList 
            data={data.items}
            renderItem={(skill) => <ItemSkill skill={skill.item} /> }
            keyExtractor={(item)=>(item.id)} />
          </View>
        
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    backgroundColor: 'white',
  }, 
  headHai: {
    marginLeft: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  headHaiDalam: {
    marginLeft: 10,
    flexDirection: 'column',
  },
  textHai: {
    fontSize: 16,
  },
  textHai2: {
    fontSize: 20,
    color: '#003366'
  },
  head2:{
    marginHorizontal: 20,
    marginTop: 30,
    borderBottomWidth: 5,
    borderBottomColor: '#3EC6FF',
  },
  textHead2:{
    fontSize: 50,
    fontWeight: 'bold',
    color: '#003366'
  },
  kotak1:{
    flexDirection: 'row',
    marginHorizontal: 15,
    marginBottom: 8
  },
  textKotak1:{
    color: '#003366',
    marginTop: 8,
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    marginHorizontal: 4,
    padding: 5,
    fontSize: 13,
    fontWeight: 'bold',
    width: '95%',
  },
  list:{
    flex: 1,
  }

});