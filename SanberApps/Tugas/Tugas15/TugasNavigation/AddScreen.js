import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const AddScreen = () => {
  return (
    <View style={styles.container}>
      <Text>Add Screen</Text>
    </View>
  )
}

export default AddScreen

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center'
  }
})
