import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';


export default class LoginScreen extends React.Component{
  render() {
    return(
      <View style={styles.container}>
        <Image source={require('./images/logo.png')} style={{height: 100, width: 300, margin: 25}} />
          <Text style={styles.textHeader}>Login</Text>
          <View style={styles.body}>
            <Text>Username / Email</Text>
            <TextInput style={styles.input}></TextInput>
            <Text>Password</Text>
            <TextInput style={styles.input}></TextInput>
          </View>
          <View style={styles.boxButtonM}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Login')}>
              <Text style={styles.textButton}>Login Screen</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.boxButtonP}>
            <Text style={styles.textPisah}>Atau</Text>
          </View>
          <View style={styles.boxButton}>
          <TouchableOpacity
          onPress={() => this.props.navigation.navigate('App',
            {
              screen: 'Home', params:
                { screen: 'Skill', params: { test: 'Mukhlis' } }
            })}>
            <Text style={styles.textButton}>Menuju Skill Screen</Text>
        </TouchableOpacity>

          </View>
            
        </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white'
  }, 
  textHeader: {
    fontSize: 25,
    alignSelf: 'center',
    marginTop: 25
  },
  body: {
    margin: 25
  },
  input: {
    borderWidth: 1,
    width: 280,
    padding: 5,
    marginBottom: 15
  },
  button: {
    justifyContent: 'space-around'
  },
  boxButton: {
    padding: 8,
    backgroundColor: '#003366',
    borderRadius: 15,
    height: 50,
    width: 150,
  },
  boxButtonP: {
    padding: 8,
    paddingHorizontal: 25,
  },  
  boxButtonM: {
    padding: 8,
    backgroundColor: '#3EC6FF',
    borderRadius: 15,
    height: 50,
    width: 150,
  },
  textButton: {
    fontSize: 22,
    textAlign: 'center',
    color: 'white'
  },
  textPisah: {
    fontSize: 22,
  }
});