import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const ProjectScreen = () => {
  return (
    <View style={styles.container}>
      <Text>Project Screen</Text>
    </View>
  )
}

export default ProjectScreen

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center'
  }
})
