import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Login from './LoginScreen';
import About from './AboutScreen';
import Skill from './Skill';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';

const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const TabsScreen = () => (
  <Tabs.Navigator tabBarOptions={{
    activeTintColor: '#e91e63',
  }}>
    <Tabs.Screen name="Skill" component={Skill} options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}/>
    <Tabs.Screen name="Project" component={ProjectScreen} options={{
          tabBarLabel: 'Updates',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="bell" color={color} size={size} />
          ),
          tabBarBadge: 3,
        }}/>
    <Tabs.Screen name="Add" component={AddScreen} options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
        }}/>
  </Tabs.Navigator>
);

const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Home">
    <Drawer.Screen name="Home" component={TabsScreen} />
    <Drawer.Screen name="About" component={About} />
  </Drawer.Navigator>
);

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="Login" component={Login} />
    <RootStack.Screen name="App" component={DrawerScreen} />
  </RootStack.Navigator>
);


const index = () => {
  return(
    <NavigationContainer>
      <RootStackScreen/>
    </NavigationContainer>
  //   <NavigationContainer>
  
  //   <Tabs.Navigator initialRouteName={Login}>
  //   <Tabs.Screen name="Skill" component={Login} />
  //   <Tabs.Screen name="Project" component={ProjectScreen} />
  //   <Tabs.Screen name="Add" component={AddScreen} />
  // </Tabs.Navigator>
  //   </NavigationContainer>
  
  
  )
}

export default index
// const LoginStackScreen = () => (
//   <Stack.Navigator>
//     <Stack.Screen name="Login" component={Login} />
//     <Stack.Screen name="Skill" component={MainApp} />
//   </Stack.Navigator>
// )

// const MyDrawwer = ()=>{
//   return(
//   <Drawer.Navigator >
//       <Drawer.Screen name="About" component={About} />
//   </Drawer.Navigator>
//   )
// }

// const MainApp =() =>{
//   return(
//     <Tabs.Navigator>
//       <Tabs.Screen name="Skill" component={Skill} />
//       <Tabs.Screen name="Project" component={ProjectScreen} />
//       <Tabs.Screen name="Add" component={AddScreen} />
//     </Tabs.Navigator>
//   )
// }