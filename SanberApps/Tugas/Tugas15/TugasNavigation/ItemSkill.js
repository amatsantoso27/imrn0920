import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class ItemSkill extends React.Component{
  render(){
    let skill=this.props.skill;
    return(
      <View style={styles.container}>
        <Icon style={{color:'#003366'}} name={skill.iconName} size={80}/>
        <View style={styles.detail}>
          <Text style={styles.textName}>{ skill.skillName}</Text>
          <Text style={styles.textCategory}>{ skill.categoryName} </Text>
          <Text style={styles.textPersen}>{ skill.percentageProgress} </Text>
        </View>
        <Icon style={{color:'#003366'}} name="chevron-right" size={80} />
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    paddingLeft: 5,
    marginTop: 10,
    flexDirection: "row",
    backgroundColor: '#B4E9FF',
    marginHorizontal: 15,
    borderRadius: 20,
    alignItems: 'center'
  },
  detail: {
    marginLeft: 10,
    width: 200
    
  },
  textName:{
    fontSize: 30,
    fontWeight: "bold",
    color: '#003366'
  },
  textCategory:{
    fontSize: 20,
    fontWeight: "bold",
    color: '#3EC6FF'
  },
  textPersen:{
    fontSize: 50,
    fontWeight: "bold",
    alignSelf: 'flex-end',
    color: 'white'

  }
});