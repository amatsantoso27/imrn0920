import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Colors } from 'react-native/Libraries/NewAppScreen';

export default class Apps extends React.Component{
  render() {
    return(
      <ScrollView>

      <View style={styles.container}>
        <Text style={styles.textHeader}>Tentang Saya</Text>
        <Icon style={{color: '#EFEFEF', textAlign: 'center'}} solid name="user-circle" size={200} />
        <Text style={styles.nama}>Achmad Santoso</Text>
        <Text style={styles.kerjaan}>Front End Developer</Text> 
        <View style={styles.kotak}>
          <Text style={styles.judulDalam}>Portofolio</Text>
          <View style={styles.kotakDalam}>
            <View>
              <Icon color= '#3ec6ff' name="github" size={40} style={styles.icon}/>
              <Text style={styles.textDalam}>@amatsantoso</Text>
            </View>
            <View>
              <Icon color= '#3ec6ff' name="gitlab" size={40} style={styles.icon}/>
              <Text style={styles.textDalam}>@amatsantoso</Text>
            </View>
          </View>
        </View>  
        <View style={styles.kotak}>
          <Text style={styles.judulDalam}>Hubungi Saya</Text>
          <View style={styles.kotakDalamVer}>
            <View style={styles.kotakDalamVerHub}>
              <View>
                <Icon color= '#3ec6ff' name="facebook" size={40} style={styles.icon}/>
              </View>
                <View style={styles.textNama}>
                  <Text style={styles.textDalam}>@amatsantoso</Text>
                </View>
            </View>
            <View style={styles.kotakDalamVerHub}>
              <View>
                <Icon color= '#3ec6ff' name="instagram" size={40} style={styles.icon}/>
              </View>
                <View style={styles.textNama}>
                  <Text style={styles.textDalam}>@amatsantoso</Text>
                </View>
            </View>
            <View style={styles.kotakDalamVerHub} >
              <View>
                <Icon color= '#3ec6ff' name="twitter" size={40} style={styles.icon}/>
              </View>
                <View style={styles.textNama}>
                  <Text style={styles.textDalam}>@amatsantoso</Text>
                </View>
            </View>
            
          </View>
        </View>  
            
      </View>
      </ScrollView> 
    )
  }
};

const styles = StyleSheet.create({
  container: {
    marginTop: 64,
  }, 
  textHeader: {
    fontSize: 36,
    textAlign: 'center',
    color: '#003366',
    fontWeight: 'bold'
  },
  icon: {
    textAlign: 'center'
  },
  nama: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#003366',
    textAlign: 'center'
  },
  kerjaan: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#3ec6ff',
    textAlign: 'center',
    marginBottom: 6
  },
  kotak: {
    borderColor: 'blue',
    borderRadius: 10,
    borderBottomColor: '#000',
    padding: 5, 
    backgroundColor: '#efefef',
    marginBottom: 9,
  },
  kotakDalam: {
    borderTopWidth: 2,
    borderTopColor: '#003366',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  kotakDalamVer: {
    borderTopWidth: 2,
    borderTopColor: '#003366',
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  kotakDalamVerHub: {
    height: 50,
    marginBottom: 2,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  judulDalam: {
    fontSize: 18,
    color: '#003366'
  },
  textDalam:{
    fontSize: 16,
    fontWeight: 'bold',
    color: '#003366',
    textAlign: 'center'
  },
  input: {
    height: 40,
    borderColor: 'grey',
    borderWidth: 1,
  },
  body: {
    margin: 25
  },
  input: {
    borderWidth: 1,
    width: 280,
    padding: 5,
    marginBottom: 15
  },
  textNama: {
    justifyContent: 'center',
    marginLeft: 10
  }
});