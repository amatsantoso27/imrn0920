import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';

const Login = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.Head}>
          <Text style={styles.textHead1}>Welcome Back</Text>
          <Text style={styles.textHead2}>Sign in to continue</Text>
        </View>
        <View style={styles.body}>
          <View>
            <Text>Email</Text>
            <TextInput style={styles.inputan}></TextInput>
          </View>
          <View>
            <Text>Password</Text>
            <TextInput style={styles.inputan}></TextInput>
          </View>
          <TouchableOpacity onPress={() => navigation.push('Home')}>
            <View style={styles.footer}>
              <Text style={styles.textFoot}>Sign In</Text>
            </View>
          </TouchableOpacity>
          <Text style={styles.or}>-OR-</Text>
          <View style={styles.footer2}>
            <TouchableOpacity onPress={() => navigation.push('Home')}>
              <View style={styles.viaLogin0}>
                <Image
                  source={require('./fb.png')}
                  style={{width: 20, height: 20, marginRight: 5}}
                />
                <Text>Facebook</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.push('Home')}>
              <View style={styles.viaLogin}>
                <Image
                  source={require('./google.png')}
                  style={{width: 40, height: 40}}
                />
                <Text>Google</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F6F7',
  },
  Head: {
    marginTop: 60,
    marginHorizontal: 15,
  },
  textHead1: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#0C0423',
    shadowColor: 'black',
    textShadowRadius: 30,
  },
  textHead2: {
    fontSize: 15,
    color: '#4D4D4D',
  },
  body: {
    marginHorizontal: 20,
    marginTop: 20,
    paddingVertical: 40,
    paddingHorizontal: 20,
    borderRadius: 25,
    backgroundColor: 'white',
    marginBottom: 100,
    paddingBottom: 40,
  },
  inputan: {
    borderBottomColor: '#E6EAEE',
    borderBottomWidth: 1,
    marginBottom: 30,
  },
  footer: {
    backgroundColor: '#F77866',
    borderRadius: 11,
    paddingVertical: 10,
    paddingHorizontal: 70,
  },
  textFoot: {
    fontSize: 25,
    textAlign: 'center',
    color: 'white',
  },
  footer2: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 30,
  },
  textFoot2: {
    fontSize: 15,
  },
  textFootWarna: {
    fontSize: 15,
    color: '#F77866',
    marginLeft: 5,
  },
  or: {
    textAlign: 'center',
    fontSize: 20,
    marginVertical: 20,
  },
  viaLogin0: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    marginHorizontal: 15,
    paddingHorizontal: 30,
    borderRadius: 10,
    borderColor: '#E6EAEE',
    paddingVertical: 10
  },
  viaLogin: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    marginHorizontal: 15,
    paddingHorizontal: 30,
    borderRadius: 10,
    borderColor: '#E6EAEE',
  },
});
