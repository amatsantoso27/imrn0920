import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Ionicons';

const Home = () => {
  return (
    <View>
      <View>
        <View style={styles.search}>
          <Icon name="search" size={10} />
          <TextInput placeholder="Search Product"></TextInput>
          <Icon2 name="camera-outline" size={10} />
        </View>
        <Icon2 name="camera-outline" size={10} />
      </View>
    </View>
  )
}

export default Home

const styles = StyleSheet.create({})
