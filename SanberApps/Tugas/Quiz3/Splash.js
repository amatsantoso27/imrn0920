import React, { useEffect } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

const Splash = ({navigation}) => {

  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Register')
    }, 3000);
  }, [])

  return (
    <View style={styles.container}>
      <View style={styles.circle}>
      <Image source={require('./logoo.jpg')} resizeMode="contain" style={{width: 200}} />
      </View>
      {/* <Image source={require} resizeMode="contain" style={{width: 200}} /> */}
    </View>
  )
}

export default Splash

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    flex:1,
    alignItems: "center",
    backgroundColor: '#F4F6F7',
  },
  circle: {
    width: 250,
    height: 250,
    borderRadius: 250/2,
    backgroundColor: '#E5E5E5',
    alignItems: "center",
    justifyContent: "center",
  }
})
