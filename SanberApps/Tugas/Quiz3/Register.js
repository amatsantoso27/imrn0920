import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {color} from 'react-native-reanimated';

const Register = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.Head}>
          <Text style={styles.textHead1}>Welcome</Text>
          <Text style={styles.textHead2}>Sign up to continue</Text>
        </View>
        <View style={styles.body}>
          <View>
            <Text>Name</Text>
            <TextInput style={styles.inputan}></TextInput>
          </View>
          <View>
            <Text>Email</Text>
            <TextInput style={styles.inputan}></TextInput>
          </View>
          <View>
            <Text>Email</Text>
            <TextInput style={styles.inputan}></TextInput>
          </View>
          <View>
            <Text>Phone Number</Text>
            <TextInput style={styles.inputan}></TextInput>
          </View>
          <View>
            <Text>Password</Text>
            <TextInput style={styles.inputan}></TextInput>
          </View>
          <TouchableOpacity onPress={() => navigation.push('Home')}>
            <View style={styles.footer}>
              <Text style={styles.textFoot}>Sign Up</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.footer2}>
            <Text style={styles.textFoot2}>Already have an account?</Text>
            <TouchableOpacity onPress={() => navigation.push('Login')}>
            <Text style={styles.textFootWarna}>Sign In</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F6F7',
  },
  Head: {
    marginTop: 60,
    marginHorizontal: 15,
  },
  textHead1: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#0C0423',
    shadowColor: 'black',
    textShadowRadius: 30,
  },
  textHead2: {
    fontSize: 15,
    color: '#4D4D4D',
  },
  body: {
    marginHorizontal: 20,
    marginTop: 20,
    paddingVertical: 30,
    paddingHorizontal: 20,
    borderRadius: 25,
    backgroundColor: 'white',
    marginBottom: 100
  },
  inputan: {
    borderBottomColor: '#E6EAEE',
    borderBottomWidth: 1,
    marginBottom: 30,
  },
  footer: {
    backgroundColor: '#F77866',
    borderRadius: 11,
    paddingVertical: 10,
    paddingHorizontal: 70,
  },
  textFoot: {
    fontSize: 25,
    textAlign: 'center',
    color: 'white',
  },
  footer2:{
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 8
  },
  textFoot2:{
    fontSize: 15,
  },
  textFootWarna:{
    fontSize: 15,
    color: '#F77866',
    marginLeft: 5
  }
  
});
